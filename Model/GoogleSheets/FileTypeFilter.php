<?php
/**
 * Created by PhpStorm.
 * User: matyushasv
 * Date: 2020-02-03
 * Time: 15:40
 */

namespace Vaimo\EmployeeBonuses\Model\GoogleSheets;
use \Magento\Config\Model\Config\Backend\File;

class FileTypeFilter extends File
{
    /**
     * @return string
     */
    public function _getAllowedExtensions()
    {
        return 'json';
    }
}