<?php
namespace Vaimo\EmployeeBonuses\Observer;

use \Vaimo\EmployeeBonuses\Model\Bonus\LoyaltySystemManager;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;

class GetBonusAmount implements ObserverInterface
{
    private $loyaltySystemManager;

    public function __construct(
        LoyaltySystemManager $loyaltySystemManager
    ) {
        $this->loyaltySystemManager = $loyaltySystemManager;
    }

    public function execute(Observer $observer)
    {
        $this->loyaltySystemManager->applyCustomerBonusAmount();
    }
}