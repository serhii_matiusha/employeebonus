<?php
/**
 * Created by PhpStorm.
 * User: matyushasv
 * Date: 2020-01-29
 * Time: 11:11
 */

namespace Vaimo\EmployeeBonuses\Model\GoogleSheets;

use \Google_Client;
use \Google_Service_Sheets;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Magento\Store\Model\ScopeInterface;

class GoogleSheetsIntegrator
{
    private $googleClient;
    private $googleService;
    private $customerListRange;
    private $scopeConfig;
    private $spreadsheetsId;
    private $informationPositioning;
    private $googleAccountKeyFilePath;

    public function __construct(
        Google_Client $googleClient,
        Google_Service_Sheets $googleServiceSheets,
        ScopeConfigInterface $scopeConfig
    ) {
        $this->googleClient = $googleClient;
        $this->googleService = $googleServiceSheets;
        $this->googleClient->useApplicationDefaultCredentials();
        $this->googleClient->addScope('https://www.googleapis.com/auth/spreadsheets');
        $this->googleService = $googleServiceSheets;
        $this->scopeConfig = $scopeConfig;
        $this->informationPositioning = $this->scopeConfig->getValue('google_sheets_api_configuration/sheets_positioning', ScopeInterface::SCOPE_STORE);
        $this->spreadsheetsId = $this->scopeConfig->getValue('google_sheets_api_configuration/sheets_access_configuration/spreadsheets_id', ScopeInterface::SCOPE_STORE);
        $this->googleAccountKeyFilePath = './pub/media/GoogleSheetsConfig/'. $this->scopeConfig->getValue('google_sheets_api_configuration/sheets_access_configuration/service_account_config');
        putenv('GOOGLE_APPLICATION_CREDENTIALS=' . $this->googleAccountKeyFilePath);

    }

    /**
     * @param $customerEmail
     *
     * @return mixed
     */
    public function getCustomerId($customerEmail)
    {
        $this->customerListRange = $this->informationPositioning['table_name'] . '!' . $this->informationPositioning['employees_column'] . $this->informationPositioning['employees_first_row'] . ':' . $this->informationPositioning['employees_column'] . '1000';
        $result = $this->getByRange($this->customerListRange);
        for ($i = 0; sizeof($result['values']) > $i; $i++) {
            if (strtolower($result['values'][$i][0]) == strtolower($customerEmail)) {
                return (int)$this->informationPositioning['employees_first_row'] + $i;
            };
        }

        return false;
    }

    /**
     * @param $customerId
     *
     * @return bool|string
     */
    public function getSingleCustomerRange($customerId)
    {
        if ($customerId) {
            return $this->informationPositioning['table_name'] . '!' . $this->informationPositioning['value_column'] . $customerId;
        }

        return false;
    }

    public function getBonusAmount($customerEmail)
    {
        $customerId = $this->getCustomerId($customerEmail);
        $response = $this->getByRange($this->getSingleCustomerRange($customerId));

        return $response['values'][0][0];
    }

    /**
     * @param $customerEmail
     * @param $bonusAmount
     */
    public function updateBonusAmount($customerEmail, $bonusAmount)
    {
        $customerId = $this->getCustomerId($customerEmail);
        $range = $this->getSingleCustomerRange($customerId);
        if ($range) {
            $range = "Лист1!B2";
            $values = [
                [$bonusAmount]
            ];
            $body = new \Google_Service_Sheets_ValueRange(['values' => $values]);
            $options = array('valueInputOption' => 'RAW');
            $this->googleService->spreadsheets_values->update($this->spreadsheetsId, $range, $body, $options);
        }
    }

    /**
     * @param $range
     *
     * @return mixed
     */
    private function getByRange($range)
    {
        return $this->googleService->spreadsheets_values->get($this->spreadsheetsId, $range);
    }
}

//    support information
//    const API_KEY = 'AIzaSyD-eH3SPHZXg1jdDFtfmwvTbYlwNpXvLNU';
//    private $clientID ='494186285158-0r66mcn9tq1l5ojajff6c6dih5ofl6ft.apps.googleusercontent.com';
//    private $secretCode = 'cJiVGpNO0_HRESIB0bq4khfw';