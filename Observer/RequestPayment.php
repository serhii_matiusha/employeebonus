<?php
/**
 * Created by PhpStorm.
 * User: matyushasv
 * Date: 2020-01-31
 * Time: 10:21
 */

namespace Vaimo\EmployeeBonuses\Observer;

use \Vaimo\EmployeeBonuses\Model\Bonus\LoyaltySystemManager;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\Event\Observer;

class RequestPayment implements ObserverInterface
{
    private $loyaltySystemManager;

    public function __construct(
        LoyaltySystemManager $loyaltySystemManager
    ) {
        $this->loyaltySystemManager = $loyaltySystemManager;
    }

    public function execute(Observer $observer)
    {
        $this->loyaltySystemManager->paymentRequest();
    }
}