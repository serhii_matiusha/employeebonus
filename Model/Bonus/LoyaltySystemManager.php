<?php
/**
 * Created by PhpStorm.
 * User: matyushasv
 * Date: 2020-01-27
 * Time: 14:47
 */

namespace Vaimo\EmployeeBonuses\Model\Bonus;

use \Magento\Customer\Model\Session;
use \Vaimo\EmployeeBonuses\Model\GoogleSheets\GoogleSheetsIntegrator;

class LoyaltySystemManager
{
    private $customerSession;
    private $googleSheetsIntegrator;
    private $customerEmail;


    public function __construct(Session $customerSession,
                                GoogleSheetsIntegrator $googleSheetsIntegrator)
    {
        $this->customerSession = $customerSession;
        $this->googleSheetsIntegrator = $googleSheetsIntegrator;
//        $this->customerEmail = $this->customerSession->getCustomer()->getEmail();
        $this->customerEmail = 'maTYUshasv@gmail.com'; //for testing without login
    }

    /**
     * taking bonus from google sheets and set it into session
     */
    public function applyCustomerBonusAmount()
    {
        $bonusAmount = $this->googleSheetsIntegrator->getBonusAmount($this->customerEmail);
        $this->customerSession->setBonusAmount($bonusAmount);
    }

    /**
     * @return bool
     */
    public function paymentRequest()
    {
        $bonusAmount = $this->googleSheetsIntegrator->getBonusAmount($this->customerEmail);
        $orderPrice = 10;  //defined while have no configured payment process and events
        $balance = ((int)$bonusAmount - (int)$orderPrice);
        if ($balance >= 0) {
            $this->googleSheetsIntegrator->updateBonusAmount($this->customerEmail, $balance);
            $this->customerSession->setBonusAmount($balance);
            return true;
        }
        return false;
    }
}